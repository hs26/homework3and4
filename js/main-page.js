var today = new Date();
var time = today.getHours() + ":" + today.getMinutes();

var username = localStorage.getItem("lastname");

if(!username){
    
    window.location.href = "../index.html";
}

$(".name").html(username);

var notCompleted = "fa-circle";
var completed = "fa-check-circle";



localToDo = localStorage.getItem("todo");

if(localToDo){
   
    todoArray = JSON.parse(localStorage.getItem("todo"));
    index = todoArray.length;
    
    
    for(var i = 0; i < index; i++){
        
        if (todoArray[i].completed == false){
            
            var className = notCompleted;
            
            }
        
        else{
            
            var className = completed;
        }
        
        if(todoArray[i].deleted == true){
            
            continue;
        }
        
        else{
        
            $(".todoOrderList").append(


                `<li class="task">
                                    <i class='far ${className}' do = 'dotask' id=${i}></i>

                                    <span class='marg'>${todoArray[i].name}</span>

                                    <i class='far fa-trash-alt' do='trash' id=${i}></i>

                    </li>`



            );
            
        }
        
        
    }
  
}

else{
    
    var todoArray = [];
    var index = 0;
    
    
}

timeChek = parseInt(today.getHours());
var check = 0;


if( timeChek >= 12 ) {
//    console.log(timeChek);
    
    $(".greeting").html("Good Afternoon, " + localStorage.getItem("lastname"));
    check = 0;
}

if( timeChek < 12 ) {
    
    $(".greeting").html("Good Morning, " + localStorage.getItem("lastname"));
}

if( timeChek > 15 ) {
    
    $(".greeting").html("Good Evening, " + localStorage.getItem("lastname"));
}

$(".ttime").html("<h1 class='time'>" + time + "</h1>");



if(timeChek ==0 && localStorage.getItem("focus") !="" ){
    
    if(check==0){
        localStorage.removeItem("focus");
        $(".inputt").show; 
        check = check + 1;
        $(".mainFocus").html("What's your main focus today?");
    
    }
}




if(localStorage.getItem("focus")!="" && localStorage.getItem("focus")!=null){
    
    $(".inputt").hide();
    $(".mainFocus").html("Today");
    $(".focus").html("<h5>" + localStorage.getItem("focus") + "<span><img class='cllose' src='../images/closee.png'></span>" + "</h5>");
}

$(".focusfrm").submit(function(e){
    
    e.preventDefault();
    if($(".input_focus").val()!=""){
        
//        console.log("hello");
        
        localStorage.setItem("focus", $(".input_focus").val());
//        console.log(localStorage.getItem("focus"));

        $(".inputt").hide();
        $(".mainFocus").html("Today");
        $(".focus").html(localStorage.getItem("focus"));
        
        $(".cllose").show();
        
        $(".cllose").html("<img src='../images/closee.png'>");
        
        $(".input_focus").val("");
        
    }
    
})

$(".cllose").click(function(){
    
    localStorage.removeItem("focus");
    $(".inputt").show();
    $(".focus").html("");
    $(".mainFocus").html("What's your main focus today?");
    $(".cllose").hide();
})



$(".todoForm").submit(function(e){
    
    e.preventDefault();
    if($(".todoInput").val()!="" && $(".todoInput").val()!=null ){
        
        
        var value = $(".todoInput").val();
        
        todoArray.push({
            
            name: value,
            index: index,
            deleted:false,
            completed:false   
            
        });
        
        
        localStorage.setItem("todo", JSON.stringify(todoArray));
    
        
        $(".todoOrderList").append(
            
        
            `<li class="task">
                                <i class='far ${notCompleted}' do = 'dotask' id=${index}></i>
                                
                                <span class='marg'>${value}</span>


                                <i class='far fa-trash-alt' do='trash' id=${index}></i>

                </li>`
        
        
        
        );
        
        index++;
        
//        console.log(localStorage.getItem("todo"));
        $(".todoInput").val("");
        
        
    }
    
})

function taskCompleted(listItem){
    
    
    listItem.classList.toggle(notCompleted);
    listItem.classList.toggle(completed);
    
    if(todoArray[listItem.attributes.id.value].completed ==true) {
        
        todoArray[listItem.attributes.id.value].completed=false;
    }
    
    else{
        
        todoArray[listItem.attributes.id.value].completed=true;
//        console.log(todoArray[listItem.attributes.id.value]);
    }
    
    localStorage.setItem("todo", JSON.stringify(todoArray));
}


function taskDeleted(listItem){
    
    
    listItem.parentNode.parentNode.removeChild(listItem.parentNode);
    todoArray[listItem.attributes.id.value].deleted = true;
    localStorage.setItem("todo", JSON.stringify(todoArray));
    
}


$(".todoOrderList").click(function(event){
    
    var listItem = event.target;
    //console.log(listItem);
    
    var task = listItem.attributes.do.value;
    
    if(task=="dotask"){
        
        taskCompleted(listItem);
        
    }
    
    else if(task=="trash"){
        
        taskDeleted(listItem);
        
    }
    
    
    
});




$(function(){
    
    
    $.ajax({

            type: 'GET',

            url: 'https://quotes.rest/qod.json',


            success: function(data){

//                console.log(data);
                $(".quote-body-text").html(data.contents.quotes[0].quote);
            
            }

        });
    
    
});