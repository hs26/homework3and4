# Name of the app #

FOCUS

### Description ###

FOCUS is the clone of the popular Google Chrome extension **MOMENTUM**. It can do almost everything Momentum can do.

* You can write your current focus 
* You can also make a TO-DO list 
* Also, you can view the current time and 
* It also shows an inspirational quote of the day

### Student Names and Ids ###

* Himank Sahni (N01293673)

### Technologies Used ###

* HTML
* CSS
* JavaScript
* jQuery
* Bootstrap

### API USED ###

https://quotes.rest/

### Instructions to run the app ###

1. Open index.html in any browser of your choice
2. If you are opening the file in your browse for the first time, then you have to enter you name
3. After bntering your name you will be redirected to Focus main screen
4. Here you can add your focus by entering it into the **What's your main focus today?**
5. You can remove your focus by clicking on the cross icon right beside to it
6. You can make your TO-DO list by clicking on the To Do icon on the top left corner


### Screenshot ###

https://bitbucket.org/hs26/homework3and4/src/master/screenshot.png

### Video ###

https://bitbucket.org/hs26/homework3and4/src/master/video.mp4
